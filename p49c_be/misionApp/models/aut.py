from django.db import models
from .user import User

class Aut(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='account', on_delete=models.CASCADE)
    marca = models.CharField('Marca', max_length= 50)
    modelo = models.CharField('Modelo', max_length= 50)
    cilindraje = models.CharField('Cilindraje', max_length= 50)
    placa = models.CharField('Placa', max_length= 50)

