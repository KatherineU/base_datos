from misionApp.models.aut import Aut
from rest_framework import serializers

class AutSerializer(serializers.ModelSerializer):
    class Meta:
     model = Aut
    fields = ['marca', 'modelo', 'cilindraje', 'placa']