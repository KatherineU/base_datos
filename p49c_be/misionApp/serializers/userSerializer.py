from rest_framework import serializers
from misionApp.models.user import User
from misionApp.models.aut import Aut
from misionApp.serializers.autSerializer import AutSerializer

class UserSerializer(serializers.ModelSerializer):
    aut = AutSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'telefono','aut']

def create(self, validated_data):
    autData = validated_data.pop('aut')
    userInstance = User.objects.create(**validated_data)
    Aut.objects.create(user=userInstance, **autData)
    return userInstance

def to_representation(self, obj):
    user = User.objects.get(id=obj.id)
    aut = Aut.objects.get(user=obj.id)
    return {
                'id': user.id,
                'username': user.username,
                'name': user.name,
                'email': user.email,
                'telefono':user.telefono,
                'aut': {
                    'id': aut.id,
                    'username': aut.username,
                    'password': aut.password,
                    'name': aut.name,
                    'email': aut.email, 
                    'telefono':aut.telefono
                }
            }