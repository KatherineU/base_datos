from django.apps import AppConfig


class MisionappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'misionApp'
